'''[how to use]
$ python3 yt-download.py
'''

from google_images_download import google_images_download   #importing the library

response = google_images_download.googleimagesdownload()   #class instantiation

search = "driver seat belt" # keywords separated by ,

arguments = {"keywords":search,"limit":1000,"print_urls":True}   #creating list of arguments
paths = response.download(arguments)   #passing the arguments to the function
print(paths)   #printing absolute paths of the downloaded images

''' [FAQs]
$ pip3 install selenium

For Linux

1. Check you have installed latest version of chrome browser-> "chromium-browser -version"
2. If not, install latest version of chrome "sudo apt-get install chromium-browser"
3. Get the appropriate version of chrome driver from http://chromedriver.storage.googleapis.com/index.html
4. Unzip the chromedriver.zip
5. Move the file to /usr/bin directory sudo mv chromedriver /usr/bin
6. Goto /usr/bin directory and you would need to run something like "chmod a+x chromedriver" to mark it executable.
7. finally you can execute the code.

from selenium import webdriver
driver = webdriver.Chrome()
driver.get("http://www.google.com")
display.stop()
'''
