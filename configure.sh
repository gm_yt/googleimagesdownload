# install selenium package
pip3 install selenium

# install google chrome
sudo apt-get install chromium-browser
wget http://chromedriver.storage.googleapis.com/80.0.3987.106/chromedriver_linux64.zip
unzip chromedriver_linux64.zip
cp chromedriver /usr/local/bin
chmod a+x /usr/local/bin/chromedriver
